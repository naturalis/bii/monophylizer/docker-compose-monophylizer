docker-monophylizer
====================

Docker compose file and docker file for running monophylizer


Contents
-------------
Dockerfile in folder container creates the naturalis/monophylizer container



Instruction building image
-------------
No special instructions.

```
docker build naturalis/monophylizer:0.0.1 .
```

Instruction running docker-compose.yml
-------------

The repository is compatible with the puppet naturalis/puppet-docker_compose manifest and can be deployed using that manifests. 

#### preparation
- Copy env.template to .env and adjust variable is wanted
- Copy traefik/traefik.env.toml to traefik/traefik.toml and adjust where needed


````
docker-compose up -d
````

Usage
-------------
Apache can be accessed directly on port 8080
Traefik dashboard can be accessed on port 8081
If there is a valid traefik.toml with or without SSL then both services can be accessed through port 80/443. 
It is advised to setup firewall rules and only allow 80/443 to the server running the docker-compose project, use port 8080,9000 and 8081 using a SSH tunnel.

